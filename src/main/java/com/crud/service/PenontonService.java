package com.crud.service;

import java.util.Optional;

import com.crud.model.Penonton;
import com.crud.repo.PenontonRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PenontonService {
    
    @Autowired
    private PenontonRepo repo;

    public Iterable<Penonton> findAll() {
        return repo.findAll();
    }

    public Penonton save(Penonton penonton){
        return repo.save(penonton);
    }

    public void deleteById(int id){
         repo.deleteById(id);
    }

    public Optional<Penonton> findById(int id){
        return repo.findById(id);
    }
    
}
