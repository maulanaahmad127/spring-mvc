package com.crud.controller;


import com.crud.model.Film;
import com.crud.model.Penonton;
import com.crud.service.FilmService;
import com.crud.service.PenontonService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

    @Autowired
    private FilmService filmService;

    @Autowired
    private PenontonService penontonService;

    @GetMapping
    public String view(Model model){
        model.addAttribute("films", filmService.findAll());
        model.addAttribute("penontons", penontonService.findAll());
        return "index";
    }

    @GetMapping("/add")
    public String add(Model model){
        model.addAttribute("penontons", new Penonton());
        model.addAttribute("listFilm", filmService.findAll());
        return "add";
    }

    @GetMapping("/addFilm")
    public String addFilm(Model model){
        model.addAttribute("films", new Film());
        return "addFilm";
    }

    @PostMapping("/save")
    public String save(Model model , Penonton penonton){
        model.addAttribute("penontons", penonton );
        penontonService.save(penonton);
        return "redirect:/";
    }

    @PostMapping("/saveFilm")
    public String saveFilm(Model model , Film film){
        model.addAttribute("films", film );
        filmService.save(film);
        System.out.println(film.getTanggal());
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id){
        penontonService.deleteById(id);
        return "redirect:/";
    }

    @GetMapping("/deleteFilm/{id}")
    public String deleteFilm(@PathVariable("id") int id){
        filmService.deleteById(id);
        return "redirect:/";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable("id") int id, Model model, Penonton penonton){
        model.addAttribute("penontons", penontonService.findById(id));
        model.addAttribute("listFilm", filmService.findAll());
        return "edit";
    }

    @GetMapping("/updateFilm/{id}")
    public String updateFilm(@PathVariable("id") int id, Model model, Film film){
        model.addAttribute("films", filmService.findById(id));
        return "editFilm";
    }

    @PostMapping("/update")
    public String updated(Model model , Penonton penonton){
        penontonService.save(penonton);
        return "redirect:/";
    }

    @PostMapping("/updateFilm")
    public String updatedFilm(Model model , Film film){
        filmService.save(film);
        return "redirect:/";
    }
    
}
